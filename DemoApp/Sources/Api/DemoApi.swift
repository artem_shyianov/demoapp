//
//  MockableApi.swift
//  DemoApp
//
//  Created by ashy on 5/7/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//

import Foundation
import Moya

enum DemoApi {
    case login(email: String, password: String)
    case getActivity(page: Int, term: String)
    case getVideo(videoId: String)
    case addComment(text: String)
    case error()
}

extension DemoApi : TargetType {
    var baseURL: URL {
        return URL(string: "http://demo6993520.mockable.io/")!
    }
    
    var path: String {
        switch self {
        case .login(_,_):
            return "login"
        case .getActivity(_,_):
            return "activity"
        case .getVideo(_):
            return "video"
        case .addComment(_):
            return "comment"
        case .error():
            return "error"
        }
    }
    
    var parameters: [String: Any]? {
        guard let user = UserStorage.default.user else { return nil }
        switch self {
        case .getActivity(let page, let term):
            return [
                "page" : page,
                "searchTerm" : term,
                "token" : user.token
            ]
        case .getVideo(let videoId):
            return [
                "id" : videoId,
                "token" : user.token
            ]
        case .addComment(let text):
            return [
                "text" : text,
                "token" : user.token
            ]
        case .login(let email, let password):
            return [
                "email" : email,
                "password" : password,
            ]
        case .error():
            return nil
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login(_,_):
            return .post
        case .addComment(_):
            return .put
        default:
            return .get
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        return .requestPlain
    }
}
