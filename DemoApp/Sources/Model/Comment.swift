//
//  Comment.swift
//  DemoApp
//
//  Created by ashy on 5/7/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//

import Mapper

class Comment: Mappable {
    var commentId: Double!
    var text: String?
    var author: String?
    var timestamp: Double?
    
    required init(map: Mapper) throws {
        try commentId = map.from("id")
        text = map.optionalFrom("text")
        author = map.optionalFrom("author")
        timestamp = map.optionalFrom("ts")
    }
    
    init(text: String) {
        self.text = text
        self.timestamp = Date().timeIntervalSince1970
        self.author = UserStorage.default.user?.firstName
    }
}
