//
//  User.swift
//  DemoApp
//
//  Created by ashy on 5/8/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//

import Mapper

class User: Mappable {
    var firstName: String?
    var lastName: String?
    var email: String!
    var token: String!
    
    required init(map: Mapper) throws {
        try email = map.from("email")
        try token = map.from("token")
        firstName = map.optionalFrom("firstName")
        lastName = map.optionalFrom("lastName")
    }
}
