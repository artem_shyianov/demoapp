//
//  Video.swift
//  DemoApp
//
//  Created by ashy on 5/7/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//


import Mapper

class Video: Mappable {
    var videId: String
    var author: String?
    var description: String?
    var name: String?
    var thumbURL: URL?
    var videoURL: URL?
    var timestamp: Double
    var comments: [Comment]?
    
    required init(map: Mapper) throws {
        try videId = map.from("id")
        author = map.optionalFrom("author")
        description = map.optionalFrom("description")
        name = map.optionalFrom("name")
        thumbURL = map.optionalFrom("thumb_url")
        videoURL = map.optionalFrom("url")
        try timestamp = map.from("ts")
        comments = map.optionalFrom("comments")
    }
    
}
