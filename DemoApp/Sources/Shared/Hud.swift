//
//  HUD.swift
//  DemoApp
//
//  Created by Artem Shyianov on 1/21/17.
//  Copyright © 2017 Artem Shyianov. All rights reserved.
//

import UIKit
import PKHUD
import Moya

class HUD {
    
    class func show() {
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    class func show(with error: Error?) {
        var errorMessage: String = ""
        switch error {
        case let responseError as MoyaError:
            do {
                if let body = try responseError.response?.mapJSON(),
                    let json = body as? [String: Any] {
                    errorMessage = json["error_message"] as! String
                }
            } catch {}
            break
        case .some(let error):
            errorMessage = error.localizedDescription
        default:
            errorMessage = "ErrorUnknown"
        }
        
        PKHUD.sharedHUD.contentView = PKHUDErrorView(subtitle:errorMessage)
        PKHUD.sharedHUD.show()
        PKHUD.sharedHUD.hide(afterDelay: 2)
        
    }
    
    class func hide() {
        PKHUD.sharedHUD.hide()
    }
}
