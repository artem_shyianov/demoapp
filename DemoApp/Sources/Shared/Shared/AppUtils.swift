//
//  AppUtils.swift
//  DemoApp
//
//  Created by ashy on 5/9/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//

import Foundation
import UIKit

class AppUtils {
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    class func cellHeight(width: CGFloat, video: Video, showDescription: Bool) -> CGSize {
        let constrainedSize: CGSize = CGSize(width: width, height: CGFloat(MAXFLOAT))
        let imageHeight: CGFloat = 230
        var titleHeight: CGFloat = 0
        var descriptionHeight:CGFloat = 0
        let paddings:CGFloat = 170
        if let titleString = video.name as NSString? {
            let expectedLabelSize = titleString.boundingRect(with:constrainedSize, options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), context: nil)
            titleHeight = expectedLabelSize.height
        }
        if let descriptionString = video.description as NSString? , descriptionString.length > 0, showDescription == true {
            let expectedLabelSize = descriptionString.boundingRect(with:constrainedSize, options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), context: nil)
            descriptionHeight = expectedLabelSize.height + paddings
        }
        let fullHeight = imageHeight + titleHeight + descriptionHeight
        return CGSize(width: width, height: fullHeight)
    }
    
}
