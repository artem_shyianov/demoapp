//
//  UserStorage.swift
//  DemoApp
//
//  Created by ashy on 5/9/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//

import Foundation

class UserStorage {
    static let `default` = UserStorage()
    
    var user: User?
}
