//
//  DetailsViewModel.swift
//  DemoApp
//
//  Created by ashy on 5/9/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Moya
import Alamofire
import Mapper

class DetailsViewModel {
    let provider: MoyaProvider<DemoApi>
    let elements = BehaviorRelay<[Comment]>(value: [])
    let disposeBag = DisposeBag()
    let loading = BehaviorRelay<Bool>(value: false)
    let errorSubject = PublishSubject<Swift.Error>()
    let commentlsValid: Driver<Bool>
    var video: Video
    
    init(provider: MoyaProvider<DemoApi>, commentText: Driver<String>, video: Video) {
        self.provider = provider
        self.video = video
        
        let commentValid = commentText
            .distinctUntilChanged()
            .throttle(0.3)
            .map { $0.count > 0 }
        commentlsValid = commentValid.asDriver()
        
        let request = getDetails().do(onNext: { [weak self] (video) in
            guard let `self` = self else { return }
            self.video = video
        }, onError: { [weak self] (error) in
            guard let `self` = self else { return }
            self.errorSubject.onError(error)
        }).share(replay: 1)
        let response = request.flatMap { video -> Observable<[Comment]> in
            guard let comments = video.comments else {
                return Observable.empty()
            }
            return Observable.just(comments)
        }.share(replay: 1)
        
        Observable
            .combineLatest(request, response, elements.asObservable()) { request, response, elements in
                return response
            }
            .sample(response)
            .bind(to: elements)
            .disposed(by: disposeBag)
    }
    
    func getDetails() -> Observable<Video> {
        return self.provider.rx.request(.getVideo(videoId: self.video.videId))
            .debug()
            .asObservable()
            .map(to: Video.self, keyPath: "video")
    }
    
    func postComment(commentText: String) -> Observable<Comment> {
        let request = self.provider.rx.request(.addComment(text: commentText))
        let response =
            request.debug()
            .asObservable()
            .flatMap({ (response) -> Observable<Comment> in
                let comment = Comment.init(text: commentText)
                let comments = self.elements.value
                self.elements.accept( [comment] + comments )
                return Observable.just(comment)
            })
        
        return response
    }
}
