//
//  UserViewModel.swift
//  DemoAppSample
//
//  Created by Artem Shyianov on 1/21/17.
//  Copyright © 2017 Artem Shyianov. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya
import Moya_ModelMapper

class LandingViewModel {
    
    //MARK: - Dependecies
    
    let provider: MoyaProvider<DemoApi>
    let disposeBag = DisposeBag()
    let refreshTrigger = PublishSubject<Void>()
    let loadNextPageTrigger = PublishSubject<Void>()
    let error = PublishSubject<Error>()

    let loading = BehaviorRelay<Bool>(value: false)
    let elements = BehaviorRelay<[Video]>(value: [])
    let searchText = BehaviorRelay<String?>(value: nil)
    var pageIndex:Int = 1
    
    init(provider: MoyaProvider<DemoApi>) {
        self.provider = provider
        
        let refresh = loading.asObservable()
        .sample(refreshTrigger)
        .flatMap { loading -> Observable<Int> in
            if loading {
                return Observable.empty()
            } else {
                return Observable<Int>.create { [unowned self] observer in
                    self.pageIndex = 1
                    observer.onNext(self.pageIndex)
                    observer.onCompleted()
                    return Disposables.create()
                }
            }
        }
        
        searchText.asObservable().subscribe(onNext: { [weak self] (text) in
            guard let `self` = self else { return }
            self.refreshTrigger.onNext(())
        }).disposed(by: disposeBag)
        
        let nextPage = loading.asObservable()
        .sample(loadNextPageTrigger)
        .flatMap { loading -> Observable<Int> in
            if loading {
                return Observable.empty()
            } else {
                return Observable<Int>.create { [unowned self] observer in
                    self.pageIndex += 1
                    observer.onNext(self.pageIndex)
                    observer.onCompleted()
                    return Disposables.create()
                }
            }
        }
        
        let request = Observable
            .of(refresh, nextPage)
            .merge()
            .share(replay: 1)

        let response = request.flatMap { repositories -> Observable<[Video]> in
            self.fetchVideos(searchTerm: self.searchText.value!, page: self.pageIndex)
            .do(onError: { error in
                    self.error.onNext(error)
                }).catchError({ error -> Observable<[Video]> in
                    Observable.empty()
                })
            }.share(replay: 1)

        Observable
            .combineLatest(request, response, elements.asObservable()) { request, response, elements in
                return self.pageIndex == 1 ? response : elements + response
            }
            .sample(response)
            .bind(to: elements)
            .disposed(by: disposeBag)

        Observable
        .of(request.map { _ in true },
            response.map {
                $0.count == 0
            },
            error.map { _ in false }
        )
        .merge()
        .bind(to: loading)
        .disposed(by: disposeBag)
    }
    
    //MARK: - Set up

    internal func fetchVideos(searchTerm: String, page: Int) -> Observable<[Video]> {
        return self.provider.rx.request(.getActivity(page: page, term: searchTerm))
            .debug()
            .asObservable()
            .map(to: [Video].self, keyPath: "results")
    }
    
    
}
