//
//  LoginViewModel.swift
//  DemoApp
//
//  Created by ashy on 5/8/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//

import RxSwift
import RxCocoa
import Moya
import Alamofire
import Moya_ModelMapper

struct LoginViewModel {
    let provider = MoyaProvider<DemoApi>(manager: Alamofire.SessionManager.default, plugins:[])
    
    let credentialsValid: Driver<Bool>
    
    init(emailText: Driver<String>, passwordText: Driver<String>) {
        let emailValid = emailText
            .distinctUntilChanged()
            .throttle(0.3)
            .map { AppUtils.isValidEmail(testStr: $0) }
        
        let passwordValid = passwordText
            .distinctUntilChanged()
            .throttle(0.3)
            .map { $0.count > 3 }
        
        credentialsValid = Driver.combineLatest(emailValid, passwordValid) {
            $0 && $1
        }
    }
    
    func login(email: String, password: String) -> Observable<User>  {
        return self.provider.rx.request(.login(email: email, password: password))
            .debug()
            .asObservable()
            .map(to: User.self, keyPath: "result")
    }
}
