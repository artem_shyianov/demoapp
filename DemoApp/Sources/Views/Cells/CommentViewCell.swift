//
//  CommentViewCell.swift
//  DemoApp
//
//  Created by ashy on 5/8/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//

import UIKit

class CommentViewCell: UITableViewCell {
    
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func configure(viewModel: Comment) {
        authorLabel.text = viewModel.author
        commentLabel.text = viewModel.text
        if let timestamp = viewModel.timestamp {
            let timestampDate = NSDate(timeIntervalSince1970: timestamp)
            dateLabel.text = timestampDate.prettyTimestampSinceNow()
        }
    }
}
