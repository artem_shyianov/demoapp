//
//  OfferViewCell.swift
//  DemoAppSample
//
//  Created by Artem Shyianov on 1/21/17.
//  Copyright © 2017 Artem Shyianov. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit
import AlamofireImage

class VideoViewCell: UICollectionViewCell {

    // MARK: Outlets

    @IBOutlet weak var thubmImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    
    // MARK: -

    func configure(viewModel: Video, showDescription: Bool) {
        nameLabel.text = viewModel.name
        let timestamp = NSDate(timeIntervalSince1970: viewModel.timestamp)
        timestampLabel.text = timestamp.prettyTimestampSinceNow()
        
        if showDescription {
            descriptionLabel.text = viewModel.description
        } else {
            descriptionLabel.text = nil
        }
        if let thmbURL = viewModel.thumbURL {
            thubmImageView.af_setImage(withURL: thmbURL)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thubmImageView.af_cancelImageRequest()
        thubmImageView.image = nil
    }
}
