//
//  LandingViewController.swift
//  DemoAppTest
//
//  Created by Artem Shyianov on 1/21/17.
//  Copyright © 2017 Artem Shyianov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Moya
import Alamofire
import RxAlertController

class LandingViewController: UIViewController, UICollectionViewDelegateFlowLayout {

    private let provider:MoyaProvider<DemoApi> = {
        return MoyaProvider<DemoApi>(manager: Alamofire.SessionManager.default, plugins:[])
    }()
    
    //MARK: - Dependencies
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var logoutItem: UIBarButtonItem!
    @IBOutlet weak var listItem: UIBarButtonItem!
    private var refreshControl: UIRefreshControl!
    private var viewModel: LandingViewModel!
    private let disposeBag = DisposeBag()
    private var columnsCount: Float = 1 {
        didSet {
            collectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        refreshControl = UIRefreshControl()
        collectionView.addSubview(refreshControl)
        
        viewModel = LandingViewModel(provider: self.provider)
        setupBindings()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailsVC = segue.destination as? VideoDetailsViewController
        if let indexPath = collectionView.indexPathsForSelectedItems?.last {
            detailsVC?.video = try? collectionView.rx.model(at: indexPath)
        }
    }

    // MARK: CollectionView
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(self.columnsCount))
        let width:CGFloat = CGFloat((collectionView.bounds.width - totalSpace)
            / CGFloat(self.columnsCount))
        var video: Video?
        do {
             video = try? collectionView.rx.model(at: indexPath)
        }
        let showDescription = columnsCount == 1
        if let video = video {
            return AppUtils.cellHeight(width: width, video: video, showDescription: showDescription)
        } else {
            return CGSize.zero
        }
    }
    
    // MARK: - Private
    
    private func setupBindings() {
        collectionView.rx.setDelegate(self).disposed(by: disposeBag)
        collectionView.rx_reachedBottom
            .bind(to: viewModel.loadNextPageTrigger)
            .disposed(by: disposeBag)
        
        viewModel.loading.asObservable().do(onNext: { isLoading in
                if isLoading { self.refreshControl.endRefreshing()}
            })
            .bind(to: isLoading(for: self.view))
            .disposed(by: disposeBag)
        let cellIndentifier = String(describing: VideoViewCell.self)
        viewModel.elements.asObservable()
            .bind(to: collectionView.rx.items(cellIdentifier: cellIndentifier,
                cellType: VideoViewCell.self)) { (tableView, element, cell) in
               cell.configure(viewModel: element, showDescription: self.columnsCount==1)
            }.disposed(by: disposeBag)
        
        refreshControl.rx.controlEvent(.valueChanged)
            .bind(to: viewModel.refreshTrigger)
            .disposed(by: disposeBag)
        
        searchBar.rx.text.asDriver()
            .drive(viewModel.searchText)
            .disposed(by: disposeBag)
        
        viewModel.error.asObservable().subscribe(onNext: { (error) in
            HUD.show(with: error)
        }).disposed(by: disposeBag)
        
        logoutItem.rx.tap.subscribe(onNext: { [weak self] in
            guard let `self` = self else { return }
            UIAlertController.rx.show(in: self, title: "Do you want to logout?", message: nil,
                                      buttonTitles: ["Yes", "No"])
                .subscribe(onSuccess: { [unowned self] choice in
                    if choice == 0 {
                        self.dismiss(animated: true, completion: nil)
                    }
                }).disposed(by: self.disposeBag)
        }).disposed(by: disposeBag)
        
        listItem.rx.tap.subscribe(onNext: { [weak self] in
            guard let `self` = self else { return }
            let isList = self.columnsCount == 1
            self.columnsCount = isList ? 2 : 1
            self.listItem.title = isList ? "List" : "Grid"
            self.collectionView.reloadData()
        }).disposed(by: disposeBag)
    }
}


extension LandingViewController {
    func isLoading(for view: UIView) -> AnyObserver<Bool> {
        return Binder(view, binding: { (hud, isLoading) in
            switch isLoading {
            case true:
                if self.viewModel.elements.value.isEmpty {
                    HUD.show()
                }
            case false:
                HUD.hide()
            }
        }).asObserver()
    }
}

