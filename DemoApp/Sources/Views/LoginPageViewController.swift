//
//  LoginPageViewController.swift
//  DemoApp
//
//  Created by ashy on 5/7/18.
//  Copyright © 2018 Artem Shyianov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Moya

class LoginPageViewController: UIViewController {
    // MARK: - IBOutlets
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    // MARK: - Variables
    private var viewModel: LoginViewModel!
    private let disposeBag = DisposeBag()
    
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.layer.borderWidth = 1
        viewModel = LoginViewModel(
            emailText: emailField.rx.text.orEmpty.asDriver(),
            passwordText: passwordField.rx.text.orEmpty.asDriver())
        setupBindins()
    }

    private func setupBindins() {
        viewModel.credentialsValid.drive(onNext: { [unowned self] valid in
            self.loginButton.isEnabled = valid
        }).disposed(by: disposeBag)
        
        loginButton.rx.tap
            .withLatestFrom(viewModel.credentialsValid)
            .filter { $0 }
            .flatMapLatest { [unowned self] valid -> Observable<User> in
                self.viewModel.login(email: self.emailField.text!, password: self.passwordField.text!)
                .observeOn(SerialDispatchQueueScheduler(qos: .userInteractive))
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] user in
                UserStorage.default.user = user
                self.performSegue(withIdentifier: "landing", sender: self)
            }, onError: { error in
                HUD.show(with: error)
            })
           .disposed(by: disposeBag)
    }
    
}
