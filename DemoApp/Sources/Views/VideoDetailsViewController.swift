//
//  FollowersListViewController.swift
//  DemoApp
//
//  Created by Artem Shyianov on 1/21/17.
//  Copyright © 2017 Artem Shyianov. All rights reserved.
//

import Foundation
import Moya
import Alamofire
import RxSwift
import RxCocoa
import AVKit
import RxKeyboard

class VideoDetailsViewController: UIViewController {
    let provider = MoyaProvider<DemoApi>(manager: Alamofire.SessionManager.default, plugins:[])
    
    //MARK: - Dependencies
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var footerView: UIView!
    private var viewModel: DetailsViewModel!
    var video: Video!
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        viewModel = DetailsViewModel(provider:provider,
            commentText: commentField.rx.text.orEmpty.asDriver(), video: video)
        setupBindins()
        updateFooter()
        
        navigationItem.title = video.name
        authorNameLabel.text = video.author
        descriptionView.text = video.description
        if let thumbURL = video.thumbURL {
            imageView.af_setImage(withURL: thumbURL)
        }
    }
    
    // MARK: - Private
    
    private func setupBindins() {
        let cellIndetifier = String(describing: CommentViewCell.self)
        viewModel.errorSubject.asObservable().subscribe(onNext: { (error) in
            HUD.show(with: error)
        }).disposed(by: disposeBag)
        viewModel.elements.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: cellIndetifier,
                                       cellType: CommentViewCell.self)) {
                                        (row, element, cell) in
                                        cell.configure(viewModel: element)
            }
            .disposed(by: disposeBag)
        viewModel.commentlsValid.drive(onNext: { [unowned self] valid in
            self.sendButton.isEnabled = valid
        }).disposed(by: disposeBag)
        sendButton.rx.tap
            .withLatestFrom(viewModel.commentlsValid)
            .filter { $0 }
            .flatMapLatest { [unowned self] valid -> Observable<Comment> in
                self.viewModel.postComment(commentText: self.commentField.text!)
                    .observeOn(SerialDispatchQueueScheduler(qos: .userInteractive))
            }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] _ in
                self.commentField.resignFirstResponder()
                self.commentField.text = nil
                }, onError: { error in
                    HUD.show(with: error)
            })
            .disposed(by: disposeBag)
        playButton.rx.tap.observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] _ in
                if let videoURL = self.viewModel.video.videoURL {
                    let player = AVPlayer(url: videoURL)
                    let playerController = AVPlayerViewController()
                    playerController.player = player
                    self.present(playerController, animated: true) {
                        player.play()
                }
            }
        }).disposed(by: disposeBag)
        RxKeyboard.instance.isHidden.asObservable().bind(to: tableView.rx.isScrollEnabled).disposed(by: disposeBag)
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { [unowned self] keyboardVisibleHeight in
                self.tableView.contentInset.bottom = keyboardVisibleHeight
            })
            .disposed(by: disposeBag)
    }
    
    func updateFooter() {
        var footerFrame = footerView.frame
        let width = view.frame.width
        let footerSize = AppUtils.cellHeight(width: width, video: video, showDescription: true)
        footerFrame.size.height = footerSize.height + commentField.frame.height
        footerView.frame = footerFrame
    }
}
