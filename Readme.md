
## Installation
```bash
$ git clone https://artem_shyianov@bitbucket.org/artem_shyianov/demoapp.git
$ cd DemoApp/
$ pod update
$ open DemoApp.xcworkspace 
```

## CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```
